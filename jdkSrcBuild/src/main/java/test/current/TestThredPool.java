package test.current;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: Levi
 * @Date: 2020/10/3 19:54
 */
public class TestThredPool {
    public static void main(String[] args) {
        ExecutorService  threadPool = Executors.newFixedThreadPool(5);

        threadPool.execute(new Thread());
    }
}
