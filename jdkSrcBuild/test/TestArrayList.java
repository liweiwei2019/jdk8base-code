/*
 * Copyright (c) 2017-2019. All Rights Reserved.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @author xujianfeng
 * @version $Id: TestArrayList.java,v 0.1 2019年12月18日 下午5:07 $Exp
 */
public class TestArrayList {

    public static void main(String[] args) {
        System.out.println(args); //[ljava.lang.string;@139a55  “[”代表数组， “l”代表long , "@139a55"代表哈希值
        System.out.println(args.length);  //默认长度为0
        List<String> list1 = new ArrayList();
        list1.add("list1");
        list1.add("list2");
        list1.add("list3");
        System.out.println("List-result" + list1.get(1));
    }
}
